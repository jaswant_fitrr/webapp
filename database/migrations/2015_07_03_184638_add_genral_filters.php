<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGenralFilters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
            Schema::table('clients', function ($table) {
            $table->string('ac_availability')->nullable();
            $table->string('parking_availability')->nullable();
            $table->string('locker_availability')->nullable();
            $table->string('wifi_availability')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
