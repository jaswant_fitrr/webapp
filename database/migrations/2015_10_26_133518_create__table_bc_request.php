<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBcRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::create('bcrequest', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->string('rollnumber');
            $table->string('name');
            $table->string('currentbranch');
            $table->string('cpi');
            $table->string('category');
            $table->string('preferences');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
