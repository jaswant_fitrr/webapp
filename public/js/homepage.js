$(document).ready(function() {

	/*Toggling between Signup/Login - start*/
	$('.signup-heading').click(function() {
		$('.signup-box-below-heading').removeClass('display-none');
		$('.login-box-below-heading').addClass('display-none');
		$('.signup-heading-tab').removeClass('inactive-tab');
		$('.login-heading-tab').addClass('inactive-tab');
	});

	$('.login-heading').click(function() {
		$('.login-box-below-heading').removeClass('display-none');
		$('.signup-box-below-heading').addClass('display-none');
		$('.login-heading-tab').removeClass('inactive-tab');
		$('.signup-heading-tab').addClass('inactive-tab');
	});
	/*Toggling between Signup/Login - end*/

	/*Toggling between Activity Type/Subtypes - start*/
	$(".activity-box").on("click", ".activity-name-box",function(event) {

		elem_activity_name = event.currentTarget ;
		elem_parent = elem_activity_name.closest(".activity-box") ;
		$(elem_activity_name).toggleClass('display-none');
		$(elem_parent).find(".activity-name-heading-box").toggleClass("display-none");
		$(elem_parent).find(".activity-subtypes-box").toggleClass("display-none");
	});

	$(".activity-box").on("click", ".activity-name-heading-box",function(event) {

		elem_activity_name = event.currentTarget ;
		elem_parent = elem_activity_name.closest(".activity-box") ;
		$(elem_parent).find(".activity-name-box").toggleClass("display-none");
		$(elem_activity_name).toggleClass("display-none");
		$(elem_parent).find(".activity-subtypes-box").toggleClass("display-none");
	});
	/*Toggling between Activity Type/Subtypes - end*/

	/*Slack Carousel for Partners Logos - start */
	$(document).ready(function(){
		$('.your-class').slick({
			slidesToShow: 6,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 2000,
		});
	});
	/*Slack Carousel for Partners Logos - end */

});