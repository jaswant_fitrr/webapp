$(document).ready(function() {


	$('.show-more-link a').click(function() {
		$('.about .box-details').removeClass('box-content-height-limit');
		$('.show-more-link').addClass('display-none');
		$('.show-less-link').removeClass('display-none');
	});

	$('.show-less-link a').click(function() {
		$('.about .box-details').addClass('box-content-height-limit');
		$('.show-less-link').addClass('display-none');
		$('.show-more-link').removeClass('display-none');
	});

	$('.time-slots .time-slot button').click(function() {
		$(this).toggleClass('slot-selected');
		$(this).toggleClass('slot-not-selected');
	});

});