$(document).ready(function() {

	$('.selectpicker').selectpicker('selectAll');

	$( "#datepicker" ).datepicker();
	$( "#datepicker" ).datepicker('setDate', new Date());

	$("select").change(function(){
		debugger
		$.ajaxSetup({
				headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
		});

		var filter_location = $( "#location" ).val();
		var filter_activity = $( "#activity" ).val();
		var filter_timing = $("#time-duration").val();
		var date_filter = $("#datepicker").val();
	//	alert(filter_location+filter_activity+filter_timing+date_filter);

		$.post( "/listings/servicefilter",{filter_location:filter_location,filter_activity:filter_activity,filter_timing:filter_timing,date_filter:date_filter} ,function( data ) {
	 			mydata = data['resArray'];
	 			subtypes = data['subtypes'];
	 			addSubTyppe(subtypes);
	 			//printres(data);
	 			general_filter();
	 	});

 	});

});



	/* Scrolling start
	var lastScrollTop = 0;

	$(window).scroll(function() {

		var currentScrollTop = $(window).scrollTop();

		var windowTop = $(window).scrollTop();
		var windowBottom = windowTop + $(window).height();
		var containerLeftTop = $('.container-left').offset().top;
		var containerLeftBottom = containerLeftTop + $('.container-left').height();

		diffScroll = lastScrollTop - currentScrollTop;
			
		var elem = document.getElementById("left-container");
		
		if (currentScrollTop > lastScrollTop) {
			//Scrolled Down
			if(containerLeftBottom <= windowBottom) {
				elem.style.position = "fixed";
				elem.style.bottom = "0";
			}
		} else {
			//Scrolled Up
			if(containerLeftTop <= windowTop) {
				elem.style.position = "fixed";
				elem.style.top = "50px";
				//$(".container-left").css({"bottom":diffScroll+"px"});
			}
		}

		lastScrollTop = currentScrollTop;
	});
	Scrolling end */

	// $('.time-slot button.slot-not-selected').click(function() {
	// 	$(this).addClass('slot-selected');
	// 	$(this).removeClass('slot-not-selected');
	// });

	// $('.time-slot button.slot-selected').click(function() {
	// 	$(this).removeClass('slot-selected');
	// 	$(this).addClass('slot-not-selected');
	// });


function checksubtype(){
	var tmp;
	var tmpsub;
	var checkedTypes = [];
	var mytmpdata;
	// make an array of checked subtypes 
	mytmpdata = curData;

	for(var i=0; i<subtypes.length; i++){

		var myid = "subtype_" +i;
		
		if($('#'+myid).is(':checked')){

			checkedTypes.push(subtypes[i]);
	 	
		}
	}
	// check if length of check is greater then change the res and print
	if(checkedTypes.length > 0){
		mytmpdata = curData.filter(function( obj ){
	 		tmpsub =  (obj.subtypes).split(":");
			return check_intersection(tmpsub,checkedTypes);
		});
	}

	printres(mytmpdata);

}

function general_filter(){
			
	curData = mydata;
	if($("#genral_filter_1").is(':checked')){

	 curData = curData.filter(function( obj ){
			return obj.ac_availability=="yes";
		});
	}

	if($("#genral_filter_2").is(':checked')){

	 curData = curData.filter(function( obj ){
	 
						return obj.womens_only=="yes";
			});

	}

	if($("#genral_filter_3").is(':checked')){

	 curData = curData.filter(function( obj ){
	 
						return obj.parking_availability=="yes";
			});


	}
	
	if($("#genral_filter_4").is(':checked')){

	 curData = curData.filter(function( obj ){
	 
						return obj.locker_availability=="yes";
			});

	}
	
	if($("#genral_filter_5").is(':checked')){

	 curData = curData.filter(function( obj ){
	 
						return obj.wifi_availability=="yes";
			});

	}
	// after completing genral filter check for subtype
	checksubtype();
}


function addSubTyppe(subtypes){

		var x = document.getElementById("sub_filters");
		x.innerHTML= "";
		var subfilters= "";
		var fvalue ; 
		for(var lm = 0; lm < subtypes.length; lm++){
			fvalue = subtypes[lm];
			subfilters = subfilters+	'<input type="checkbox" onclick="checksubtype()" class="subtypecheck" name="subtypes-filter" id='+"subtype_"+ lm + '><span class="filter">'+ fvalue +'</span><br>';

		}

		x.innerHTML = subfilters;

}

function check_intersection(a, b){

	var mytmp=0;
	for(var ta =0; ta < a.length; ta++){
			if (mytmp ==1)
					break;
		for(var tb =0; tb < b.length; tb++){
			if(a[ta] == b[tb]){

				mytmp =1;
				break;
			}
		}

	}

	return mytmp;

}

function printres(data ){
	var x = document.getElementById("results");
	x.innerHTML= "";
	var c =0;
	var resData = data;
	var content = "";
	while(resData.length > 0){

			c = c+1;
			var id = resData[0].client_id;
			var tmpsub = [];
			var listsub = [];
		
			var all_slots = data.filter(function( obj ) {
					if(obj.client_id == id){
						listsub =  (obj.subtypes).split(":");
						tmpsub.push(listsub[0]);
					}
					return obj.client_id == id;
			});
			content = content +'<div class="client-element"><div class="row-fluid"><div class="left col-xs-3"><a href='+'/client-details/'+resData[0].activity_id+'><img src="images/icon.jpg" class="icon"></a></div><div class="middle col-xs-4"><ul class="ul-no-extra-padding ul-no-list-style-type"><li class="name"><h4><a href='+'/client-details/'+resData[0].client_id+'>'+resData[0].name+'</a></h4></li><li class="address">'+resData[0].address+","+resData[0].area+'</li><li class="essentials"><div class="essentials-icons">';
			content = content +'<div class="essential wifi-icon"></div><div class="essential ac-icon"></div><div class="essential ladies-only-inactive-icon"></div><div class="essential locker-icon"></div><div class="essential field-inactive-icon"></div><div class="essential parking-icon"></div><div class="essential shower-icon"></div><div class="essential kids-inactive-icon"></div>';
			

			var essential = "";

			if(resData[0].wifi_availability == "yes"){

				essential = essential + '<div class="essential wifi-icon"></div>';
			}
			// laduies only to be added 

			if(resData[0].locker_availability == "yes"){
				essential = essential + '<div class="essential locker-icon"></div>';
			}
			if(resData[0].parking_availability == "yes"){
				essential = essential + '<div class="essential parking-icon"></div>';
			}
			if(resData[0].ac_availability == "yes"){
				essential = essential + '<div class="essential ac-icon"></div>'
			}


			content = content+ '</div></li></ul></div><div class="right col-xs-5"><ul class="ul-no-extra-padding ul-no-list-style-type"><li id="slots-heading">AVAILABLE SLOTS</li><li class="time-slots"><ul class="inline ul-no-extra-padding ul-no-list-style-type row">';
			// content = content +'<div class="client-element"> <div class="row-fluid"> <div class="left col-xs-3">  <a href='+'/client-details/'+resData[0].activity_id+'> <img src="images/icon.jpg" class="icon"> </a> </div> <div class="middle col-xs-4"> <ul class="ul-no-extra-padding ul-no-list-style-type"> <li class="name"><h4><a href='+'/client-details/'+resData[0].client_id+'>'+resData[0].name+'</a></h4></li> <li class="address">'+resData[0].address+" "+resData[0].area +resData[0].activity_id+'</li> <li class="featured-amenities"> <ul class="inline ul-no-extra-padding ul-no-list-style-type"> <!--Assuming there are 4 Features--> <li>Air Conditioned</li> <li>Parking</li> <li>Locker</li> <li>Personal Trainer</li> <li>24 * 7 Open</li> </ul> </li> </ul> </div> <div class="right col-xs-5"> <ul class="ul-no-extra-padding ul-no-list-style-type"> <li id="slots-heading">AVAILABLE SLOTS</li> <li class="time-slots"><ul class="inline ul-no-extra-padding ul-no-list-style-type row">' 
		 	var slotcontent = "";

			 for(var j=0; j<all_slots.length; j++){
			 	slotcontent =slotcontent + '<li class="col-xs-4 time-slot"><button class="btn btn-block white-box slot-not-selected"><div class="slot-time">'+all_slots[j].startTime.substr(0,5)+'-' +all_slots[j].endTime.substr(0,5)+'</div><div class="sub-type">' + tmpsub[j] +'</div></button></li>';			 	

			 	// /* To extract slots date from the database. Don't delete. */
			 	// slotcontent =slotcontent + '<li class="time-slot col-xs-4"><button class="btn btn-block slot-not-selected">'+all_slots[j].startTime.substr(0,5)+'-' +all_slots[j].endTime.substr(0,5)+'</button></li>';
				
				}
				
			var classes = (resData[0].conductsClasses).split(":");
			var classcontent = "";
			for(var ci =0; ci<classes.length; ci++){
				classcontent = classcontent + '<li class="offering">'+ classes[ci] +'</li>';
			}	
			content = content+slotcontent + '</ul></li></ul></div></div><div class="row-fluid"><div class="col-xs-7"><div class=""><div class="offerings"><ul class="ul-no-list-style-type ul-no-extra-padding inline-display">'+classcontent+'</ul></div></div></div><div class="col-xs-3 col-xs-offset-2"><button class="btn btn-block book-button">BOOK</button></div></div></div>'
			
			// content =content+slotcontent +'</ul></li> <li class="book-button"> <div class="row"> <div class="col-xs-4 col-xs-offset-8"> <button class="btn btn-block slot-selected">BOOK</button> </div> </div> </li> <!--<li class="book-button"><button class="btn btn-primary btn-block">Book</button></li>--> </ul> </div> </div> </div>';
		
			resData = resData.filter(function( obj ){
				return obj.client_id!==id;
			});
	}

	if(c==0){
		x.innerHTML = "Sorry no result found";
	}
	else
	{
		x.innerHTML = content;
	}
	
}
