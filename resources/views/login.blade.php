@extends('layouts.master')

@section('title', 'CS251')

@section('resources')
<?php
 if (session_status() == PHP_SESSION_NONE) {
    session_start();
    }
    if(isset($_SESSION['user']) && !empty($_SESSION['user'])) {
        $newURL = "/admin";
        header('Location: '.$newURL);
        die();
    }
 ?>
 <meta name="_token" content="{{ csrf_token() }}"/>
	<link rel="stylesheet" href="{{ Helper::get_Path('css/partner.css') }}">
	<script src="{{ Helper::get_Path('js/partner.js') }}"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
@stop

@section('header')

@stop

@section('content')
<!--Cover photo section - start -->
<script>

$(document).ready(function(){
	$("input").keypress(function(event) {
			
    	if (event.which == 13) {
        	letMeIn();
        	//$("form").submit();
    	}
	});
});

	 function letMeIn(){

        debugger
        $.ajaxSetup({
                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });

     
        var username = $( "#_user_login" ).val();
        var password = $("#_user_pswd").val();


      	
            $.post( "/login",{username:username,password:password} ,function( data ) {
                var success = data['success'];
                var confirm = data['confirm'];
                if (success == 1){
                		if(username == "admin"){
                			window.location.replace("/admin");	
                		}
                		else{
                				window.location.replace("/branchchange");
                		}
                    	
                   
                }
                else{

                   
                    alert("Wrong email or password");

                }

            });
      


       
    }



</script>

	
<div class="cover-photo">

	<div class="row">

		<!-- Signin box - start -->
		<div class="col-xs-7">
		<div class="login-signup">
			<div class="box">
				<div class="heading-row">
						<div class="heading-row-content login-heading">LOGIN</div>
				</div>

				<div class="box-below-heading login-box-below-heading">
					<div class="login-row">
						<input type="text" name="username" id="_user_login" placeholder="USERNAME" class="username-row">
					</div>

					<div class="login-row">
						<input type="password" name="password" id="_user_pswd" placeholder="PASSWORD" class="password-row">
					</div>

					<div class="login-row login-button-row">
						<div class="row">
							<div class="col-xs-12 col-xs-offset-0">
								<!-- <div class="btn btn-block login-button" onclick="letMeIn()">LOGIN</div> -->
								<button type="button" onclick="letMeIn()" class="btn btn-block login-button">LOGIN</button>
							</div>
						</div>
					</div>

				</div>
			</div>

		</div>
		</div>
		<!-- Signin box - end -->

	</div>
</div>
<!--Cover photo section - end -->



@stop

