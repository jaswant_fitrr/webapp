@extends('layouts.master')

@section('title', 'Listings Page')

@section('resources')
	<meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="{{ Helper::get_Path('css/listings.css') }}">
    <script src="{{ Helper::get_Path('js/listings.js') }}"></script>
    <link rel="stylesheet" href="{{ Helper::get_Path('/css/external/bootstrap-select.css') }}">
    <script src="{{ Helper::get_Path('/js/external/bootstrap-select.js') }}"></script>
    <link rel="stylesheet" href="{{ Helper::get_Path('/css/icons.css') }}">
	
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script>

	</script>

	<script type="text/javascript">
	// storing results in mydata
		var mydata;
		var curData;
		var subtypes;

		$(document).ready(function() {
			$.ajaxSetup({
   				headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
			});

			var filter_location = '<?php  echo($location); ?>';
			var filter_activity = '<?php  echo($activity); ?>';
			var filter_timing = '<?php  echo($timeduration); ?>';;
			var date_filter =  '<?php echo($defadate); ?>';

			$.post( "/listings/servicefilter",{filter_location:filter_location,filter_activity:filter_activity,filter_timing:filter_timing,date_filter:date_filter} ,function( data ) {
	  	 		mydata = data['resArray'];
	  	 		subtypes = data['subtypes'];
	  	 		addSubTyppe(subtypes);
	  	 	//	printres(data);
	  	 		general_filter();
			});
		});
	</script>

	<script>
	// genral filters
	$(document).ready(function() {	

		$(".mycheck").click(function () {
			
				curData = mydata;
		if($("#genral_filter_1").is(':checked')){

		 curData = curData.filter(function( obj ){
		 
							return obj.ac_availability=="yes";
				});
		}

		if($("#genral_filter_2").is(':checked')){

		 curData = curData.filter(function( obj ){
		 
							return obj.womens_only=="yes";
				});

		}
	
		if($("#genral_filter_3").is(':checked')){

		 curData = curData.filter(function( obj ){
		 
							return obj.parking_availability=="yes";
				});


		}
		
		if($("#genral_filter_4").is(':checked')){

		 curData = curData.filter(function( obj ){
		 
							return obj.locker_availability=="yes";
				});

		}
		
		if($("#genral_filter_5").is(':checked')){

		 curData = curData.filter(function( obj ){
		 
							return obj.wifi_availability=="yes";
				});

		}
		// after completing genral filter check for subtype
		checksubtype()

   		});



	});

	</script>


@stop

@section('header')
	@include('partials.header', ['headertype' => 1])
@stop

@section('content')
	<div class="below-header">
		<div class="search-page">
			<div class="row-fluid clearfix">

				<div class="container-left col-xs-2" id="left-container">
					<div class="left-filters">

						<div class="filters-block">
							<div class="filters-heading">
								<h4><strong>General Filters</strong></h4>
							</div>
							<div class="up">
								<form action="" class="filter-form general-filters">
									<input type="checkbox" class="mycheck" name="general-filter" id= "genral_filter_1" value="general-filter-1">
										<span class="filter">AC Available</span><br>
									<input type="checkbox" class="mycheck" name="general-filter" id= "genral_filter_2" value="general-filter-2">
										<span class="filter">Ladies Only Sessions</span><br>
									<input type="checkbox" class="mycheck" name="general-filter" id= "genral_filter_3" value="general-filter-3">
										<span class="filter">Parking Available</span><br>
									<input type="checkbox" class="mycheck" name="general-filter" id= "genral_filter_4" value="general-filter-4">
										<span class="filter">Locker Available</span><br>
									<input type="checkbox" class="mycheck" name="general-filter" id= "genral_filter_5" value="general-filter-5">
										<span class="filter">Wifi Available</span><br>
								</form>
							</div>
						</div>
						
						<div class="filters-block">
							<div class="filters-heading">
								<h4><strong>Sub Types</strong></h4>
							</div>
							<div class="down subtypes-filters">
								<form action="" class="filter-form" id="sub_filters">
									
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="container-right col-xs-10">
					<div id="top-filters">
						<div class="row-fluid">

							<div class="top-filter col-xs-2">
								<div class="form-group">
									<label for="search">SEARCH</label>
									<input type="text" class="form-control" id="search" placeholder="Search a class">
								</div>
							</div>

							<div class="top-filter col-xs-2">
								<div class="form-group">
									<label for="location">LOCATION</label>

									<!--Location select with Selectpicker - start-->
									<select id="location" class="form-control selectpicker">
										@foreach($areas as $area)
										<option value="{{$area->locality}}">{{$area->locality}}</option>
										@endforeach
									</select>
									<!--Location select with Selectpicker - end-->

									<!--Location select - normal old way-->
									<!--<select class="form-control" id="location">
										@foreach($areas as $area)
										<option value="{{$area->locality}}">{{$area->locality}}</option>
										@endforeach
									</select>-->
								</div>
							</div>

							<div class="top-filter col-xs-2">
								<div class="form-group">
									<label for="datepicker">DATE</label>
									<input type="text" class="form-control" id="datepicker">
								</div>
							</div>
							
							<!--Ordinary time filter-->
							<!--<div class="top-filter col-xs-2">
								<div class="form-group">
									<label for="time-duration">Time</label>
									<select class="form-control" id="time-duration">
										<option value="8:30-11:30">8:30-11:30</option>
											<option value="10:00-13:00">10:00-13:00</option>
											<option value="17:00-20:00">17:00-20:00</option>
											<option value="18:30-21:30">18:30-21:30</option>
									</select>
								</div>
							</div>-->

							<!--Time filter with Timepicker plugin-->
							<!--<div class="top-filter col-xs-2">
								<div class="form-group">
									<label for="time-duration">START TIME</label>
									<input type="text" class="form-control" id="time-duration">
								</div>
							</div>-->

							<!--Time filter with Bootstrap-select plugin - start-->
							<div class="top-filter col-xs-2">
								<div class="form-group">
									<label for="time-duration">TIME</label>
									<select id="time-duration" class="form-control selectpicker" multiple data-size="4">
										<option value="8:00-11:00">8:00-11:00</option>
										<option value="9:00-12:00">9:00-12:00</option>
										<option value="10:00-13:00">10:00-13:00</option>
										<option value="11:00-14:00">11:00-14:00</option>
										<option value="12:00-15:00">12:00-15:00</option>
										<option value="13:00-16:00">13:00-16:00</option>
									</select>
								</div>
							</div>
							<!--Time filter with Bootstrap-select plugin - end-->

							<div class="top-filter col-xs-2">
								<div class="form-group">
									<label for="activity">ACTIVITY</label>

									<!--Activity select with Selectpicker - start-->
									<select id="activity" class="form-control selectpicker" multiple>
										@foreach($activities as $activity)
										<option value="{{$activity->id}}">{{$activity->type}}</option>
										@endforeach
									</select>
									<!--Activity select with Selectpicker - end-->

									<!--Activity select - normal old way-->
									<!--<select class="form-control" id="activity">
										@foreach($activities as $activity)
										<option value="{{$activity->id}}">{{$activity->type}}</option>
										
										@endforeach
										<option value="0">Any</option>
									</select>-->
								</div>
							</div>
						</div>
					</div>

					<div id="results">
					</div>


					<!--Hardcoded result to code/test - start-->
					<!--<div id="results">
						<div class="client-element">
							<div class="row-fluid">
								<div class="left col-xs-3">
									<a href="http://fitrr.herokuapp.com/client-details/1"><img src="images/icon.jpg" class="icon"></a>
								</div>
								<div class="middle col-xs-4">
									<ul class="ul-no-extra-padding-margin ul-no-list-style-type">
										<li class="name"><h4><a href="http://fitrr.herokuapp.com/client-details/2">yoga for life</a></h4></li>
										<li class="address">Kailash Tower, 8 Th Floor - 804, Powai, Mumbai - 400076, Behind S M Shetty School,Opposite Panchshru.,Powai</li>
										<li class="essentials">
											<div class="essentials-icons">
												<div class="essential wifi-icon"></div>
												<div class="essential ac-icon"></div>
												<div class="essential ladies-only-inactive-icon"></div>
												<div class="essential locker-icon"></div>
												<div class="essential field-inactive-icon"></div>
												<div class="essential parking-icon"></div>
												<div class="essential shower-icon"></div>
												<div class="essential kids-inactive-icon"></div>
											</div>
										</li>
									</ul>
								</div>
								<div class="right col-xs-5">
									<ul class="ul-no-extra-padding-margin ul-no-list-style-type">
										<li id="slots-heading">AVAILABLE SLOTS</li>
										<li class="time-slots">
											<ul class="inline ul-no-extra-padding-margin ul-no-list-style-type row">
												<li class="col-xs-4 time-slot"><button class="btn btn-block white-box slot-not-selected"><div class="slot-time">11:00-12:00</div><div class="sub-type">Power Yoga</div></button></li>
												<li class="col-xs-4 time-slot"><button class="btn btn-block white-box slot-not-selected"><div class="slot-time">09:00-10:00</div><div class="sub-type">Power Yoga</div></button></li>
												<li class="col-xs-4 time-slot"><button class="btn btn-block white-box slot-not-selected"><div class="slot-time">09:00-10:00</div><div class="sub-type">Power Yoga</div></button></li>
												<li class="col-xs-4 time-slot"><button class="btn btn-block white-box slot-not-selected"><div class="slot-time">09:00-10:00</div><div class="sub-type">Power Yoga</div></button></li>
												<li class="col-xs-4 time-slot"><button class="btn btn-block white-box slot-not-selected"><div class="slot-time">09:00-10:00</div><div class="sub-type">Power Yoga</div></button></li>
												<li class="col-xs-4 time-slot"><button class="btn btn-block white-box slot-not-selected"><div class="slot-time">09:00-10:00</div><div class="sub-type">Power Yoga</div></button></li>
											</ul>
										</li>
									</ul>
								</div>
							</div>
							<div class="row-fluid">
								<div class="col-xs-7">
									<div class="">
										<div class="offerings">
											<ul class="ul-no-list-style-type ul-no-extra-padding-margin inline-display">
												<li class="offering">Power Yoga</li>
												<li class="offering">Weight Training</li>
												<li class="offering">Group Classes</li>
												<li class="offering">Open on Sundays</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="col-xs-3 col-xs-offset-2">
									<button class="btn btn-block book-button">BOOK</button>
								</div>
							</div>
						</div>
					</div>-->
					<!--Hardcoded result to code/test - start-->



				</div>
			</div>
		</div>
	</div>
@stop

@section('footer')
	@include('partials.footer')
@stop
