@extends('layouts.master')

@section('title', 'Client Dashboard')

@section('resources')
    <link rel="stylesheet" href="{{ Helper::get_Path('css/dashboard-schedule.css') }}">

    <link rel="stylesheet" href="{{ Helper::get_Path('/css/external/bootstrap-select.css') }}">
    <script src="{{ Helper::get_Path('/js/external/bootstrap-select.js') }}"></script>
@stop

@section('header')
	@include('partials.header', ['headertype' => 2, 'id' => $id])
@stop

@section('content')
	<div class="below-header">
		<div class="mid-page">
			<div class="table-block">
				<div class="table-headings-row">
					<div class="row">
						<div class="col-xs-2">
						</div>
						<div class="col-xs-8">
							<div class="row">
								<div class="col-xs-3">
									<div class="heading-box">Name</div>
								</div>
								<div class="col-xs-3">
									<div class="heading-box">Activity</div>
								</div>
								<div class="col-xs-2">
									<div class="heading-box">Date</div>
								</div>
								<div class="col-xs-2">
									<div class="heading-box">Time</div>
								</div>
								<div class="col-xs-2 last-column">
									<div class="heading-box">Booking ID</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="table-headings-border-bottom">
				</div>

				<div class="table-entries-block">
					<div class="row">

						<!--Code to extract from back end
						@foreach($totalBookings as $booking)
						<div class="table-entries-row before-today">
							<div class="row">
								<div class="col-xs-3">
									<div class="table-entry">{{$booking->username}}</div>
								</div>
								<div class="col-xs-3">
									<div class="table-entry">High Intensity Crossfit</div>
								</div>
								<div class="col-xs-2">
									<div class="table-entry">{{$booking->slot_date}}</div>
								</div>
								<div class="col-xs-2">
									<div class="table-entry">{{$booking->startTime}}-{{$booking->endTime}}</div>
								</div>
								<div class="col-xs-2 last-column">
									<div class="table-entry">{{$booking->booking_id}}</div>
								</div>
							</div>
						</div>
						@endforeach-->

						<div class="col-xs-2">
							<div class="selection-area">
								<div class="selection-aread-element">
									<div class="selection-heading">MONTH</div>
									<!--<div class="month-select">June</div>-->
									<select id="month" class="selectpicker" data-width="150px" multiple data-size="6" title='Select Month ...'>
										<option value="january">January</option>
										<option value="february">February</option>
										<option value="march">March</option>
										<option value="april">April</option>
										<option value="may">May</option>
										<option value="june">June</option>
										<option value="july">July</option>
										<option value="august">August</option>
										<option value="september">September</option>
										<option value="october">October</option>
										<option value="november">November</option>
										<option value="december">December</option>
									</select>
								</div>
								<div class="selection-area-element">
									<div class="selection-heading">SLOT</div>
									<!--<div class="time-slot-select">All</div>-->
									<select id="slot" class="selectpicker" data-width="150px" multiple data-size="6" title='Select Slot ...'>
										<option value="8:00-11:00">8:00-11:00</option>
										<option value="9:00-12:00">9:00-12:00</option>
										<option value="10:00-13:00">10:00-13:00</option>
										<option value="11:00-14:00">11:00-14:00</option>
										<option value="12:00-15:00">12:00-15:00</option>
										<option value="13:00-16:00">13:00-16:00</option>
									</select>
								</div>
								<div class="selection-area-element">
									<div class="selection-heading">ACTIVITY</div>
									<!--<div class="activity-select">All</div>-->
									<select id="activity" class="selectpicker" data-width="150px" multiple title='Select Activity ...'>
										<option value="gym">Gym</option>
										<option value="yoga">Yoga</option>
										<option value="dance">Dance</option>
										<option value="kickboxing">Kick-boxing</option>
									</select>
								</div>
							</div>
						</div>

						<div class="col-xs-8">
						<!--Hardcoded - start -->
						<div class="table-entries-row before-today">
							<div class="row">
								<div class="col-xs-3">
									<div class="table-entry">Amit Kumar</div>
								</div>
								<div class="col-xs-3">
									<div class="table-entry">Weight Training</div>
								</div>
								<div class="col-xs-2">
									<div class="table-entry">23-06-2015</div>
								</div>
								<div class="col-xs-2">
									<div class="table-entry">06:30-07:30</div>
								</div>
								<div class="col-xs-2 last-column">
									<div class="table-entry">AF2317893</div>
								</div>
							</div>
						</div>

						<div class="table-entries-row today">
							<div class="row">
								<div class="col-xs-3">
									<div class="table-entry">Rohit Jain</div>
								</div>
								<div class="col-xs-3">
									<div class="table-entry">Weight Training</div>
								</div>
								<div class="col-xs-2">
									<div class="table-entry">28-06-2015</div>
								</div>
								<div class="col-xs-2">
									<div class="table-entry">08:00-09:00</div>
								</div>
								<div class="col-xs-2 last-column">
									<div class="table-entry">AF2783825</div>
								</div>
							</div>
						</div>

						<div class="table-entries-row today">
							<div class="row">
								<div class="col-xs-3">
									<div class="table-entry">Ron Philip</div>
								</div>
								<div class="col-xs-3">
									<div class="table-entry">Power Yoga</div>
								</div>
								<div class="col-xs-2">
									<div class="table-entry">28-06-2015</div>
								</div>
								<div class="col-xs-2">
									<div class="table-entry">18:30-19:30</div>
								</div>
								<div class="col-xs-2 last-column">
									<div class="table-entry">AF3518863</div>
								</div>
							</div>
						</div>
						<!--Hardcoded - end -->
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('footer')
	@include('partials.footer')
@stop