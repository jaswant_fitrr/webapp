@extends('layouts.master')

@section('title', 'Client Dashboard')

@section('resources')
	<meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="{{ Helper::get_Path('css/dashboard-slots.css') }}">

    <!--For Datepicker jquery plugin-->
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script>
		$(function() {
			$( "#datepicker1" ).datepicker();
			$( "#datepicker2" ).datepicker();
			$( "#datepicker3" ).datepicker();
		});
	</script>


	<script>
		 function addNewSlot(){


			$.ajaxSetup({
   					headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
				});

				var startTime = $("#startTime").val();
				var endTime = $("#endTime").val();
				var activity = $("#activity").val();
				var startDate = $("#datepicker1").val();
				var endDate =  $("#datepicker2").val();
				var slots = $("#no_slots").val();
				var excludeDate = $("#datepicker3").val() ;
				var cur_url = window.location.href + "/addnewslot";
				alert(cur_url);
				$.post( cur_url,{startTime:startTime, endTime:endTime, activity:activity, startDate:startDate, endDate:endDate, slots:slots, excludeDate:excludeDate} ,function( data ) {
	  	 			
	  	 			//mydata = data;
	  	 			alert(data);
	  	 			//intres();
			 	});

		 }
		

	</script>

@stop

@section('header')
	@include('partials.header', ['headertype' => 2, 'id' => $id])
@stop

@section('content')
	<div class="below-header">
		<div class="mid-page">
			<div class="table">
				<div class="table-headings-row">
					<div class="row">
						<div class="col-xs-1">
							<div class="heading-box gap">Start Time</div>
						</div>
						<div class="col-xs-1">
							<div class="heading-box gap">End Time</div>
						</div>
						<div class="col-xs-2">
							<div class="heading-box gap">Activity</div>
						</div>
						<div class="col-xs-2">
							<div class="heading-box gap">Subtype</div>
						</div>
						<div class="col-xs-1">
							<div class="heading-box gap">Start Date</div>
						</div>
						<div class="col-xs-1">
							<div class="heading-box gap">End Date</div>
						</div>
						<div class="col-xs-1">
							<div class="heading-box gap">Slots</div>
						</div>
						<div class="col-xs-2">
							<div class="heading-box gap">Excluded Dates</div>
						</div>
					</div>
				</div>

				<div class="table-entries">
					<div class="table-entries-row">
						<div class="row">
							
							<div class="col-xs-1">
								<div class="table-entry gap form-group">
									<select class="form-control" id="startTime">
										<option value="00:00">00:00</option>
										<option value="01:00">01:00</option>
										<option value="02:00">02:00</option>
										<option value="03:00">03:00</option>
										<option value="04:00">04:00</option>
										<option value="05:00">05:00</option>
										<option value="06:00">06:00</option>
										<option value="07:00">07:00</option>
										<option value="08:00">08:00</option>
										<option value="09:00">09:00</option>
										<option value="10:00">10:00</option>
										<option value="11:00">11:00</option>
										<option value="12:00">12:00</option>
										<option value="13:00">13:00</option>
										<option value="14:00">14:00</option>
										<option value="15:00">15:00</option>
										<option value="16:00">16:00</option>
										<option value="17:00">17:00</option>
										<option value="18:00">18:00</option>
										<option value="19:00">19:00</option>
										<option value="20:00">20:00</option>
										<option value="21:00">21:00</option>
										<option value="22:00">22:00</option>
										<option value="23:00">23:00</option>
										<option value="24:00">24:00</option>
									</select>
								</div>
							</div>
							
							<div class="col-xs-1">
								<div class="table-entry gap form-group">
									<select class="form-control" id="endTime">
										<option value="00:00">00:00</option>
										<option value="01:00">01:00</option>
										<option value="02:00">02:00</option>
										<option value="03:00">03:00</option>
										<option value="04:00">04:00</option>
										<option value="05:00">05:00</option>
										<option value="06:00">06:00</option>
										<option value="07:00">07:00</option>
										<option value="08:00">08:00</option>
										<option value="09:00">09:00</option>
										<option value="10:00">10:00</option>
										<option value="11:00">11:00</option>
										<option value="12:00">12:00</option>
										<option value="13:00">13:00</option>
										<option value="14:00">14:00</option>
										<option value="15:00">15:00</option>
										<option value="16:00">16:00</option>
										<option value="17:00">17:00</option>
										<option value="18:00">18:00</option>
										<option value="19:00">19:00</option>
										<option value="20:00">20:00</option>
										<option value="21:00">21:00</option>
										<option value="22:00">22:00</option>
										<option value="23:00">23:00</option>
										<option value="24:00">24:00</option>
									</select>
								</div>
							</div>

							<div class="col-xs-2">
								<div class="table-entry gap form-group">
									<select class="form-control" id="activity">
										<option value="1">Gym</option>
										<option value="2">Yoga</option>
										<option value="3">Zumba</option>
										<option value="4">Kickboxing</option>
									</select>
								</div>
							</div>
							
							<div class="col-xs-2">
								<div class="table-entry gap form-group">
									<select class="form-control" id="subtype">
										<option value="1">Power Yoga</option>
										<option value="2">Ashtanga Yoga</option>
										<option value="3">Classical Yoga</option>
									</select>
								</div>
							</div>

							<div class="col-xs-1">
								<div class="table-entry gap form-group">
									<input type="text" class="form-control" id="datepicker1">
								</div>
							</div>

							<div class="col-xs-1">
								<div class="table-entry gap form-group">
									<input type="text" class="form-control" id="datepicker2">
								</div>
							</div>
							
							<div class="col-xs-1">
								<div class="table-entry gap form-group">
									<select class="form-control" id="no_slots">
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
										<option value="8">8</option>
										<option value="9">9</option>
										<option value="10">10</option>
									</select>
								</div>
							</div>

							<div class="col-xs-2">
								<div class="table-entry gap form-group">
									<input type="text" class="form-control" id="datepicker3">
								</div>
							</div>

							<div class="col-xs-1 last-column">
								<div class="table-entry gap">
									<!--<div class="btn btn-default" onclick="addNewSlot()" >ADD</div>-->
									<div class="btn add-button" onclick="addNewSlot()" >+</div>
								</div>
							</div>
						</div>
					</div>

					

					@foreach($client_slots as $slot)
					<div class="table-entries-row">
						
						<div class="row">
							<div class="col-xs-1">
								<div class="table-entry gap">{{$slot->startTime}}</div>
							</div>
							<div class="col-xs-1">
								<div class="table-entry gap">{{$slot->endTime}}</div>
							</div>
							<div class="col-xs-2">

								<div class="table-entry gap">{{$slot->type}}</div>
							</div>
							<div class="col-xs-2">

								<div class="table-entry gap">Subtype</div>
							</div>
							<div class="col-xs-1">
								<div class="table-entry gap">{{$slot->slot_date}}</div>
								
							</div>

							<div class="col-xs-1">
								<div class="table-entry gap">{{$slot->endDate}}</div>
							</div>
							<div class="col-xs-1">

								<div class="table-entry gap">{{$slot->no_slots}}</div>

							</div>
							<div class="col-xs-2">
								<div class="table-entry gap form-group">{{$slot->ExcludeDate}}</div>
							</div>
						</div>
						
					</div>
					@endforeach

				</div>

			</div>
		</div>
	</div>
@stop

@section('footer')
	@include('partials.footer')
@stop