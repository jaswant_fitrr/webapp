@extends('layouts.master')

@section('title', 'Client Dashboard')

@section('resources')
    <link rel="stylesheet" href="{{ Helper::get_Path('css/dashboard-home.css') }}">

    <link rel="stylesheet" href="{{ Helper::get_Path('/css/external/bootstrap-select.css') }}">
    <script src="{{ Helper::get_Path('/js/external/bootstrap-select.js') }}"></script>
@stop

@section('header')
    @include('partials.header', ['headertype' => 2, 'id' => $id])
@stop

@section('content')
	<div class="below-header">
		<div class="mid-page">
			<div id="top-block">
				<div class="row">
					<div class="col-xs-9" id="revenue-block">
						<div class="white-box" id="revenue-white-box">
							<div class="row">
								<div class="col-xs-2">
									<div class="revenue-block-heading" id="heading-revenue">REVENUE</div>
									<!--<div class="revenue-block-value" id="month"><div id="month-name">June</div></div>-->
									<select id="month" class="selectpicker" data-width="125px">
										<option value="january">January</option>
										<option value="february">February</option>
										<option value="march">March</option>
										<option value="april">April</option>
										<option value="may">May</option>
										<option value="june">June</option>
										<option value="july">July</option>
										<option value="august">August</option>
										<option value="september">September</option>
										<option value="october">October</option>
										<option value="november">November</option>
										<option value="december">December</option>
									</select>
								</div>
								<div class="col-xs-2">
									<div class="revenue-block-heading">Gym</div>
									<div class="revenue-block-value">4000</div><!--Need to look for Rupees sign & commas-->
								</div>
								<div class="col-xs-2">
									<div class="revenue-block-heading">Weight Training</div>
									<div class="revenue-block-value">6000</div>
								</div>
								<div class="col-xs-2">
									<div class="revenue-block-heading">Zumba</div>
									<div class="revenue-block-value">2000</div>
								</div>
								<div class="col-xs-2">
									<div class="revenue-block-heading">Total</div>
									<div class="revenue-block-value">12000</div>
								</div>
								<div class="col-xs-2">
									<div class="revenue-block-heading">Cumulative</div>
									<div class="revenue-block-value">112000</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-xs-3" id="logged-in-as-block">
						<div class="white-box" id="logged-in-as-white-box">
							<div>Logged in as</div>
							<div id="client-name">VIIKINGS TRANCE FITNESS</div>
							<div class="btn" id="logout-button">LOG OUT</div>
						</div>
					</div>
				</div>
			</div>

			<div id="todays-clients-block">
				<div class="white-box" id="todays-clients-white-box">
					<div id="table-heading-row">
						<div id="table-heading">
							TODAY'S CLIENTS
						</div>
					</div>
					<div id="table">
						<div id="column-headings-row">
							<div class="row">
								<div class="col-xs-3 column-heading">NAME</div>
								<div class="col-xs-3 column-heading">SERVICE</div>
								<div class="col-xs-3 column-heading">SLOT</div>
								<div class="col-xs-3 column-heading">BOOKING ID</div>
							</div>
						</div>
						<div id="table-content">

							@foreach ($todaysUsers as $user)
								<div class="table-content-row">
									<div class="row">
										<div class="col-xs-3">{{$user->username}}</div>
										<div class="col-xs-3">Power Yoga</div>
										<div class="col-xs-3">{{$user->startTime}}-{{$user->endTime}}</div>
										<div class="col-xs-3">{{$user->booking_id}}</div>
									</div>
								</div>							
							@endforeach
							<div class="table-content-row">
								<div class="row">
									<div class="col-xs-3">Ravi Kumar</div>
									<div class="col-xs-3">yoga</div>
									<div class="col-xs-3">7:00-8:00</div>
									<div class="col-xs-3">ATFI2698</div>
								</div>
							</div>
							<div class="table-content-row">
								<div class="row">
									<div class="col-xs-3">Anjali Singh</div>
									<div class="col-xs-3">Power Yoga</div>
									<div class="col-xs-3">7:00-8:00</div>
									<div class="col-xs-3">ATFI2705</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<!--Content section of Old design - start-->
	{{--
	<div class="below-header">
		<div class="logged-in-as-row row">
			<div class="logged-in-as-box">Logged in as {{$client->name}}</div>
		</div>
		<div class="blocks">
			<div class="row">
				<div class="col-xs-6">
					<div class="block left-block">
						<div class="block-heading-row">
							<div class="block-heading-element">
								TODAY'S CLIENTS
							</div>
						</div>

						<!--Code to extract left block content from back end-->
						<!--<div class="left-block-content">
						@foreach ($todaysUsers as $user)
							<div class="left-block-content-row">
								<span>{{$user->username}} |</span>
									<span>High Intensity Crossfit |</span>
									<span>{{$user->startTime}}-{{$user->endTime}} |</span>
								<span>{{$user->booking_id}}</span>
							</div>
							
						@endforeach
						</div>-->

						<!--Hardcoded left block content-->
						<div class="left-block-content">
							<div class="left-block-content-row">
								<span>Anil Kumar |</span>
									<span>Classical Yoga |</span>
									<span>8:30-9:30 |</span>
								<span>AEF9303</span>
							</div>
							<div class="left-block-content-row">
								<span>Akshay Kale |</span>
									<span>Hot Yoga |</span>
									<span>17:30-19:00 |</span>
								<span>AEU9402</span>
							</div>
							<div class="left-block-content-row">
								<span>Neil Dsouza |</span>
									<span>Hot Yoga |</span>
									<span>17:30-19:00 |</span>
								<span>AEU9415</span>
							</div>
							<div class="left-block-content-row">
								<span>Rima Srivastava |</span>
									<span>Weight Training |</span>
									<span>20:00-21:00 |</span>
								<span>AEH9809</span>
							</div>
							<div class="left-block-content-row">
								<span>Anjali Saxena |</span>
									<span>Weight Training |</span>
									<span>20:00-21:00 |</span>
								<span>AEH9822</span>
							</div>
						</div>

					</div>
				</div>
				<div class="col-xs-6">
					<div class="block right-block">
						<div class="block-heading-row">
							<div class="block-heading-element">
								REVENUE
							</div>
						</div>
						<div class="right-block-content">
							<div class="row right-block-content-headings-row">
								<div class="col-xs-4 col-xs-offset-1">ACTIVITY</div>
								<div class="col-xs-3">THIS MONTH</div>
								<div class="col-xs-3">CUMULATIVE</div>
							</div>
							<div class="row right-block-content-data-row">
								<div class="col-xs-4 col-xs-offset-1">High Intensity Crossfit</div>
								<div class="col-xs-3">4000</div>
								<div class="col-xs-3">48000</div>
							</div>
							<div class="row right-block-content-data-row">
								<div class="col-xs-4 col-xs-offset-1">Weight Training</div>
								<div class="col-xs-3">6000</div>
								<div class="col-xs-3">64000</div>
							</div>
							<div class="border">
							</div>
							<div class="row right-block-content-data-row">
								<div class="col-xs-4 col-xs-offset-1">Total</div>
								<div class="col-xs-3">10000</div>
								<div class="col-xs-3">112000</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	--}}
	<!--Content section of Old design - end-->
@stop

@section('footer')
	@include('partials.footer')
@stop