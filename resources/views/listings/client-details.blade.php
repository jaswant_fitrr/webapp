@extends('layouts.master')

@section('title', 'Client Details')

@section('resources')
	<!--<link rel="stylesheet" href="{{ Helper::get_Path('css/listings.css') }}">-->
	<link rel="stylesheet" href="{{ Helper::get_Path('css/client-details.css') }}">
	<script src="{{ Helper::get_Path('js/listings.js') }}"></script>
	<script src="{{ Helper::get_Path('js/client-details.js') }}"></script>
	<link rel="stylesheet" href="{{ Helper::get_Path('css/icons.css') }}">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<!--For Google API-->
	<script src="https://maps.googleapis.com/maps/api/js"></script>
	<script>
		function initialize() {
			var mapCanvas = document.getElementById('map-location');
			var latlng = new google.maps.LatLng({{ $client->latitude }}, {{ $client->longitude }});
			var mapOptions = {
				center: latlng,
				zoom: 15,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				mapTypeControl: false,
				scrollwheel: false,
				streetViewControl: false
			}
			var map = new google.maps.Map(mapCanvas, mapOptions);

			var marker = new google.maps.Marker({
				position: latlng,
				map: map,
				title: ''
			});
		}
		google.maps.event.addDomListener(window, 'load', initialize);
	</script>


	<script>
	//var client;
		$(document).ready(function() {

		//	client = '<?php  echo($client->name); ?>';

			alert("hi");
		});

	</script>

	<!--For Photo Gallery-->
	<link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
	<link rel="stylesheet" href="{{ Helper::get_Path('css/bootstrap-image-gallery.min.css') }}">
@stop

@section('header')
	@include('partials.header', ['headertype' => 1])
@stop

@section('content')
	<div class="client-details">
		<div class="mid">
			<div class="row" id="cover-photo">

				<img src="{{URL::asset('images/'.$client->coverImage)}}" class="icon">

				<div class="header">
					<h3 class="client-name">{{$client->name}}</h3>
				</div>

			</div>
			<div class="row">
				<div class="col-xs-6">
					<div class="location white-box section-margin">
						<div class="address">{{$client->address}}</div>
						<div id="map-location"></div>
					</div>

					<div class="about white-box about-box section-margin">
						<h4 class="box-heading">About</h4>
						<div class="box-details box-content-height-limit">
							{{$client->aboutUs}}
						</div>
						<div class="show-more-link"><a class="">show more</a></div>
						<div class="show-less-link display-none"><a class="">show less</a></div>
					</div>

					<div class="photos section-margin">
						<div class="row">
							<div id="links">
								<div class="col-xs-4">
									<a href="/images/photo1.jpg" title="Dumbell" data-gallery>
										<img src="/images/photo1.jpg" alt="">
									</a>
								</div>
								<div class="col-xs-4">
									<a href="/images/photo2.jpg" title="Weights" data-gallery>
										<img src="/images/photo2.jpg" alt="Apple">
									</a>
								</div>
								<div class="col-xs-4">
									<div class="last-thumbnail">
										<a href="/images/photo3.jpg" title="Some description" data-gallery>
											<img src="/images/photo3.jpg" alt="Orange" style="position:absolute">
											<div class="plus">+2</div>
										</a>
									</div>
								</div>
								<div class="hidden-thumbnail">
									<a href="/images/photo3.jpg" title="Treadmill" data-gallery>
										<!--<img src="/images/photo3.jpg" alt="Orange">-->
									</a>
								</div>
								<div class="hidden-thumbnail">
									<a href="/images/photo1.jpg" title="Gym" data-gallery>
										<!--<img src="/images/photo1.jpg" alt="Banana">-->
									</a>
								</div>
								<div class="hidden-thumbnail">
									<a href="/images/photo2.jpg" title="Room No. 1" data-gallery>
										<!--<img src="/images/photo2.jpg" alt="Apple">-->
									</a>
								</div>
								<div class="hidden-thumbnail">
									<a href="/images/photo2.jpg" title="Some description" data-gallery>
										<!--<img src="images/photo2.jpg" alt="Orange">-->
									</a>
								</div>
								<div class="hidden-thumbnail">
									<a href="/images/photo2.jpg" title="Some description" data-gallery>
										<!--<img src="images/photo2.jpg" alt="Orange">-->
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-6">
					<div class="essentials white-box  essentials-box section-margin">
						<div class="essentials-icons">
							<div class="essential wifi-icon"></div>
							<div class="essential ac-icon"></div>
							<div class="essential ladies-only-inactive-icon"></div>
							<div class="essential locker-icon"></div>
							<div class="essential field-inactive-icon"></div>
							<div class="essential parking-icon"></div>
							<div class="essential shower-icon"></div>
							<div class="essential kids-inactive-icon"></div>
						</div>
					</div>
					
					<div class="offerings section-margin">
						<h4 class="right-side-left-margin">Offerings</h4>
						<ul class="no-list-style-type inline-display">
						@foreach($classes as $class)
							<li class="offering">{{$class}}</li>
							<!-- <li class="offering">Weight Training</li>
							<li class="offering">Group Classes</li>
							<li class="offering">Open on Sundays</li> -->
						@endforeach
						</ul>
					</div>

					<!--<div class="photos section-margin">
						<div class="row">
							 @foreach ($imageArray as $image)
      							<div class="col-xs-4">
								<img src="{{URL::asset('images/'.$image)}}" class="icon">
								</div>
   							 @endforeach
						</div>
					</div>-->

					<div class="available-slots section-margin">
						<h4 class="right-side-left-margin">Slots</h4>
						<ul class="row time-slots ul-no-extra-padding ul-no-list-style-type">
							<li class="col-xs-4 time-slot">
								<button class="btn btn-block white-box slot-not-selected">
									<div class="slot-time">2:30-3:30</div>
									<div class="sub-type">Power Yoga</div>
								</button>
							</li>
							<li class="col-xs-4 time-slot">
								<button class="btn btn-block white-box slot-not-selected">
									<div class="slot-time">3:30-4:30</div>
									<div class="sub-type">Classical Yoga</div>
								</button>
							</li>
							<li class="col-xs-4 time-slot">
								<button class="btn btn-block white-box slot-not-selected">
									<div class="slot-time">4:30-5:30</div>
									<div class="sub-type">Power Yoga</div>
								</button>
							</li>
							<li class="col-xs-4 time-slot">
								<button class="btn btn-block white-box slot-not-selected">
									<div class="slot-time">5:30-6:30</div>
									<div class="sub-type">Power Yoga</div>
								</button>
							</li>
							<li class="col-xs-4 time-slot">
								<button class="btn btn-block white-box slot-not-selected">
									<div class="slot-time">17:30-18:30</div>
									<div class="sub-type">Classical Yoga</div>
								</button>
							</li>
						</ul>
						<div class="book-button-row">
							<div class="row">
								<div class="col-xs-5 slot-date-column">
									<div class="white-box slot-date">25 - 06 - 2015</div>
								</div>
								<div class="col-xs-7">
									<button class="btn btn-block book-button">Book Slot</button>
								</div>
							</div>
						</div>
						<!--</ul>-->
					</div>

				</div>
			</div>


			<!-- Photo Gallery Code - start -->
			<!-- The Bootstrap Image Gallery lightbox, should be a child element of the document body -->
			<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-use-bootstrap-modal="false">
			    <!-- The container for the modal slides -->
			    <div class="slides"></div>
			    <!-- Controls for the borderless lightbox -->
			    <h3 class="title"></h3>
			    <a class="prev">‹</a>
			    <a class="next">›</a>
			    <a class="close">×</a>
			    <a class="play-pause"></a>
			    <ol class="indicator"></ol>
			    <!-- The modal dialog, which will be used to wrap the lightbox content -->
			    <div class="modal fade">
			        <div class="modal-dialog">
			            <div class="modal-content">
			                <div class="modal-header">
			                    <button type="button" class="close" aria-hidden="true">&times;</button>
			                    <h4 class="modal-title"></h4>
			                </div>
			                <div class="modal-body next"></div>
			                <div class="modal-footer">
			                    <button type="button" class="btn btn-default pull-left prev">
			                        <i class="glyphicon glyphicon-chevron-left"></i>
			                        Previous
			                    </button>
			                    <button type="button" class="btn btn-primary next">
			                        Next
			                        <i class="glyphicon glyphicon-chevron-right"></i>
			                    </button>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
			<!-- Photo Gallery Code - end -->


		</div>
	</div>


    <!--For Photo Gallery-->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
	<script src="{{ Helper::get_Path('js/bootstrap-image-gallery.min.js') }}"></script>
@stop

@section('footer')
	@include('partials.footer')
@stop
