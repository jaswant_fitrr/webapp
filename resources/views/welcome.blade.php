@extends('layouts.master')

@section('title', 'Fitrr')

@section('resources')
<meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="{{ Helper::get_Path('css/listings.css') }}">
    <script src="{{ Helper::get_Path('js/listings.js') }}"></script>
    <link rel="stylesheet" href="{{ Helper::get_Path('/css/external/bootstrap-select.css') }}">
    <script src="{{ Helper::get_Path('/js/external/bootstrap-select.js') }}"></script>
    <link rel="stylesheet" href="{{ Helper::get_Path('/css/icons.css') }}">
    
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script>

    </script>

    <link rel="stylesheet" href="{{ Helper::get_Path('css/homepage.css') }}">
    <script src="{{ Helper::get_Path('js/homepage.js') }}"></script>

    <!-- Carousel for Partners Logos -->
    <link rel="stylesheet" type="text/css" href="{{ Helper::get_Path('slick/slick.css') }}"/>
    <!--Add the new slick-theme.css if you want the default styling-->
    <link rel="stylesheet" type="text/css" href="{{ Helper::get_Path('slick/slick-theme.css') }}"/>
    <script type="text/javascript" src="{{ Helper::get_Path('slick/slick.min.js') }}"></script>

    <script>

    function signUp(){
        debugger
        $.ajaxSetup({
                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });

        var username = $( "#_username" ).val();
        var phone = $( "#_phone" ).val();
        var address = $("#_address").val();
        var email = $("#_email").val();
        var password = $("#_password").val();
        var re_password = $("#_re_password").val();

        if(password == re_password){
            $.post( "/signup",{username:username,phone:phone,address:address,email:email,password:password} ,function( data ) {
            
               window.location.replace("/listings");
            
            });

        }




    }



    function letMeIn(){

        debugger
        $.ajaxSetup({
                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });


        var username = $( "#_user_login" ).val();
        var password = $("#_user_pswd").val();
        alert(username);
            $.post( "/login",{username:username,password:password} ,function( data ) {
                var success = data['success'];
                if (success == 1){
                    window.location.replace("/listings");
                }
                else{

                   
                    alert("wrong password");

                }

            });



    }




    </script>
@stop

@section('header')
    
@stop

@section('content')
<!--Cover photo section - start -->
<div class="cover-photo">
    
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                        <li><a href="/" class="navbar-brand"><img src="/images/logo.png" class="header-logo"></a></li>
                </ul>
                <ul class="nav navbar-nav row pull-right">
                    <li><a href="#">FAQ</a></li>
                    <li><a href="#">CONTACT US</a></li>
                    <li><a href="#">BLOG</a></li>
                    <li><a href="/partner">FOR PARTNERS</a></li>
                </ul>
            </div>
        </div>
    </nav>


    <div class="row">

        <!--Cover photo text - start -->
        <div class="col-xs-5 cover-text">
            <div class="cover-text-heading-1"><em>ONE PASS. LIMITLESS FITNESS</em></div>
            <div class="cover-text-heading-2"><em>GET 'FITRR' TODAY</em></div>

            <div class="cover-text-paragraph">
            Fitrr is a monthly membership to the best fitness<br>
            classes in your city. There are hundreds of classes,<br>
            available to Fitrr members, including zumba, yoga<br>
            strength training, dance, martial arts and more.<br>
            </div>
            <!--<div>Fitrr is a monthly membership to the best fitness</div>
            <div>classes in your city.There are hundreds of classes,</div>
            <div>available to Fitrr members, including zumba, yoga</div>
            <div>strength training, dance, martial arts and more.</div>
            </div>-->
        </div>
        <!--Cover photo text - end -->

        <!-- Signin/signup box - start -->
        <div class="col-xs-7 col-xs-offset-3 box">
            <div class="heading-row">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="heading-row-column heading-row-left-column login-heading-tab">
                            <div class="heading-row-column-content login-heading">LOGIN</div>
                        </div>
                    </div>
                    <div CLASS="col-xs-6">
                        <div class="heading-row-column heading-row-right-column signup-heading-tab inactive-tab">
                            <div class="heading-row-column-content signup-heading">SIGNUP</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box-below-heading login-box-below-heading">
                <div class="login-row">
                    <input type="text"  id="_user_login"name="username" placeholder="USERNAME" class="username-row">
                </div>
                
                <div class="login-row">
                    <input type="password" id="_user_pswd" name="password" placeholder="PASSWORD" class="password-row">
                </div>

                <div class="login-row login-button-row">
                    <div class="row">
                        <div class="col-xs-12 col-xs-offset-0">
                            <div onclick="letMeIn()" class="btn btn-block login-button">LOGIN</div>
                        </div>
                    </div>
                </div>

                <div class="login-row fb-login-row">
                    <div class="btn btn-block fb-login-button">LOGIN WITH FACEBOOK</div>
                </div>
                <div class="login-row google-login-row">
                    <div  class="btn btn-block google-login-button">LOGIN WITH GOOGLE</div>
                </div>
            </div>

            <div class="box-below-heading signup-box-below-heading display-none">
                <div class="signup-row">
                    <input type="text" name="username" id="_username" placeholder="USERNAME" class="input-row username-row">
                </div>
                <div class="signup-row">
                    <input type="text" name="phone" id="_phone" placeholder="PHONE" class="input-row phone-row">
                </div>
                <div class="signup-row">
                    <textarea name="address" rows="4" id="_address" placeholder="ADDRESS" form="" class="input-row address-row"></textarea>
                </div>
                <div class="signup-row">
                    <input type="text" name="email" id="_email" placeholder="EMAIL" class="input-row email-row">
                </div>
                <div class="signup-row">
                    <input type="password" name="password" id="_password" placeholder="PASSWORD" class="input-row password-row">
                </div>
                <div class="signup-row">
                    <input type="password" id="_re_password" name="confirm-password" placeholder="CONFIRM PASSWORD" class="input-row confirm-password-row">
                </div>
                <div class="signup-row signup-button-row">
                    <div class="row">
                        <div class="col-xs-12 col-xs-offset-0">
                            <div class="btn btn-block signup-button" onclick="signUp()">CONFIRM</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Signin/signup box - end -->
    </div>

</div>
<!--Cover photo section - end -->

<!--How it works section - start -->
<div class="how-it-works-section">
    <div class="icons-section">
        <div class="row">
            <div class="col-xs-4 signup">
                <div><img src="/images/homepage-icons/signup-icon.png"></div>
                <div class="text-under-icon">SIGNUP</div>
                <!--<h4>SIGNUP</h4>-->
            </div>
            <div class="col-xs-4 discover">
                <div><img src="/images/homepage-icons/discover-icon.png"></div>
                <div class="text-under-icon">DISCOVER</div>
            </div>
            <div class="col-xs-4 get-fitrr">
                <div><img src="/images/homepage-icons/get-fitrr-icon.png"></div>
                <div class="text-under-icon">GET FITRR</div>
            </div>
        </div>
    </div>
    
    <div class="bullets-section">
        <div class="bullets-section-heading">
            HOW IT WORKS
        </div>
        <div class="bullets">
            <ul>
                <li>GET FITRR BY PAYING RS. 2599 (INCLUSIVE OF TAXES) A MONTH.</li>
                <li>HANDPICK WORKOUTS FROM OUR CURATED SELECTION OVER THE NEXT 30 DAYS.</li>
                <li>RESERVE A SPOT IN ANY WORKOUT OF YOUR CHOOSING USING OUR WEBSITE.</li>
                <li>DISCOVER NEW WAYS TO GET FITRR WHEREVER, WHENEVER.</li>
                <li>FEEL LIKE GETTING FITRR EVERY MONTH, SIMPLY RE-CHARGE AND GO AGAIN!</li>
            </ul>
        </div>
    </div>
</div>
<!--How it works section - end -->

<!--Activities - start -->
<div class="activities-section">
    <div class="activities-heading">
        ACTIVITIES
    </div>
    <div class="activities">
        <div class="row activities-row">
            <div class="col-xs-6">
                <div class="activity-box">
                    <img src="/images/activities/yoga.png" class="activity-image">
                    <div class="activity-name-box"><div class="activity-name">YOGA</div></div>
                    <div class="activity-name-heading-box display-none"><div class="activity-name">YOGA</div></div>
                    <div class="activity-subtypes-box display-none"><div class="activity-subtypes-names"><div class="sub-type">POWER YOGA</div><div class="sub-type">PILATES</div><div class="sub-type">AERIAL YOGA</div><div class="sub-type">HATHA YOGA</div><div class="sub-type">ARTISTIC YOGA</div></div></div>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="activity-box">
                    <img src="/images/activities/cardio.png" class="activity-image">
                    <div class="activity-name-box"><div class="activity-name">CARDIO</div></div>
                    <div class="activity-name-heading-box display-none"><div class="activity-name">CARDIO</div></div>
                    <div class="activity-subtypes-box display-none"><div class="activity-subtypes-names"><div class="sub-type">RUNNING</div><div class="sub-type">OUTDOOR CYCLING</div><div class="sub-type">SPINNING</div><div class="sub-type">AQUA CARDIO</div><div class="sub-type">RPM</div></div></div>
                </div>
            </div>
        </div>
        <div class="row activities-row">
            <div class="col-xs-6">
                <div class="activity-box">
                    <img src="/images/activities/dance.png" class="activity-image">
                    <div class="activity-name-box"><div class="activity-name">DANCE</div></div>
                    <div class="activity-name-heading-box display-none"><div class="activity-name">DANCE</div></div>
                    <div class="activity-subtypes-box display-none"><div class="activity-subtypes-names"><div class="sub-type">ZUMBA</div><div class="sub-type">AEROBICS</div><div class="sub-type">CRUSH CARDIO</div><div class="sub-type">BOLLYFIT</div><div class="sub-type">STEP AEROBICS</div></div></div>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="activity-box">
                    <img src="/images/activities/interval-training.png" class="activity-image">
                    <div class="activity-name-box"><div class="activity-name">INTERVAL TRAINING</div></div>
                    <div class="activity-name-heading-box display-none"><div class="activity-name">INTERVAL TRAINING</div></div>
                    <div class="activity-subtypes-box display-none"><div class="activity-subtypes-names"><div class="sub-type">CROSSFIT</div><div class="sub-type">BOOTCAMP</div><div class="sub-type">CIRCUIT TRAINING</div><div class="sub-type">TRX</div><div class="sub-type">TABATA</div></div></div>
                </div>
            </div>
        </div>
        <div class="row activities-row">
            <div class="col-xs-6">
                <div class="activity-box">
                    <img src="/images/activities/martial-arts.png" class="activity-image">
                    <div class="activity-name-box"><div class="activity-name">MARTIAL ARTS</div></div>
                    <div class="activity-name-heading-box display-none"><div class="activity-name">MARTIAL ARTS</div></div>
                    <div class="activity-subtypes-box display-none"><div class="activity-subtypes-names"><div class="sub-type">BOXING</div><div class="sub-type">KICK BOXING</div><div class="sub-type">KRAV MAGA</div><div class="sub-type">BODY COMBAT</div><div class="sub-type">MUAY THAI</div></div></div>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="activity-box">
                    <img src="/images/activities/gym-and-more.png" class="activity-image">
                    <div class="activity-name-box"><div class="activity-name">GYM & MORE</div></div>
                    <div class="activity-name-heading-box display-none"><div class="activity-name">GYM & MORE</div></div>
                    <div class="activity-subtypes-box display-none"><div class="activity-subtypes-names"><div class="sub-type">BODY PUMP</div><div class="sub-type">BODY CONDITIONING</div><div class="sub-type">GYMBALL</div><div class="sub-type">KETTLEBEELL</div><div class="sub-type">ABT</div></div></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Activities - end -->

<!--Our Partners - start -->
<div class="our-partners-section">
    <div class="our-partners-heading">
        OUR PARTNERS
    </div>

    <div class="our-partners">
        <div class="row your-class">
            <div class="col-xs-2">
                <img src="/images/clients-logos/logo1.jpg" class="partner-logo">
            </div>
            <div class="col-xs-2">
                <img src="/images/clients-logos/logo.jpg" class="partner-logo">
            </div>
            <div class="col-xs-2">
                <img src="/images/clients-logos/logo.jpg" class="partner-logo">
            </div>
            <div class="col-xs-2">
                <img src="/images/clients-logos/logo.jpg" class="partner-logo">
            </div>
            <div class="col-xs-2">
                <img src="/images/clients-logos/logo1.jpg" class="partner-logo">
            </div>
            <div class="col-xs-2">
                <img src="/images/clients-logos/logo.jpg" class="partner-logo">
            </div>
            <div class="col-xs-2">
                <img src="/images/clients-logos/logo.jpg" class="partner-logo">
            </div>
            <div class="col-xs-2">
                <img src="/images/clients-logos/logo.jpg" class="partner-logo">
            </div>
            <div class="col-xs-2">
                <img src="/images/clients-logos/logo1.jpg" class="partner-logo">
            </div>
            <div class="col-xs-2">
                <img src="/images/clients-logos/logo.jpg" class="partner-logo">
            </div>
        </div>
    </div>
</div>
<!--Our Partners - end -->

@stop

<!--Footer - start -->
@section('footer')
<div class="footer">
    <div class="row">
        <div class="col-xs-4 learn-more-section">
            <div class="footer-section-heading">LEARN MORE</div>
            <div class="learn-more-link"><a href="#">PRIVACY POLICY</a></div>
            <div class="learn-more-link"><a href="#">HELP CENTER</a></div>
            <div class="learn-more-link"><a href="#">TERMS</a></div>
            <div class="learn-more-link"><a href="#">CAREER</a></div>
            <div class="learn-more-link"><a href="#">MEDIA KIT</a></div>
        </div>
        <div class="col-xs-4 connect-section">
            <div class="footer-section-heading">CONNECT</div>
            <span><a href="#">Tw</a></span>
            <span><a href="#">Fb</a></span>
            <span><a href="#">G+</a></span>
            <span><a href="#">In</a></span>
        </div>
        <div class="col-xs-4 contact-section">
            <div class="footer-section-heading">CONTACT</div>
            <div>+91 9999 999999</div>
            <div class="contact-email">INFO@FITRR.IN</div>
        </div>
    </div>
</div>
@stop
<!--Footer - end -->
