@extends('layouts.master')

@section('title', 'Fitrr')

@section('resources')
 <meta name="_token" content="{{ csrf_token() }}"/>
	<link rel="stylesheet" href="{{ Helper::get_Path('css/partner.css') }}">
	<script src="{{ Helper::get_Path('js/partner.js') }}"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
@stop

@section('header')

@stop

@section('content')
<script>

function checkentry(){

		var roll = $("#roll_no").val(); 
		var currbranch = $("#curr_branch").val();
		var name = $("#name_s").val();
		var cpi = $("#cpi").val();
		var category = $("#category").val(); 
		var prefernce1= $("#prefernce1").val();
		var prefernce2 = $("#prefernce2").val();
		var prefernce3 = $("#prefernce3").val();
		var prefernce4 = $("#prefernce4").val();
		
		if(roll.length >= 2){
			var str = roll[0]+roll[1];
			if(str!="15"){
				document.getElementById("bc_button").disabled = true; 
				return 0;
			}
			else{
				if(name=="" || cpi=="" || category=="" || currbranch=="" || prefernce1=="" || prefernce2=="" || prefernce3 =="" ||prefernce4==""){
					document.getElementById("bc_button").disabled = true;

					//alert(prefernce1);
					return 0;
				}
				else{
					document.getElementById("error_div").style.display='none'; 
					document.getElementById("bc_button").disabled = false;
					 return 1;	 

				}
			}
		}


}

$(document).ready(function(){
	document.getElementById("bc_button").disabled = true;
	$("input").bind('input', function() { 
		checkentry();
	});
	$("select").change(function(){
		checkentry();
	});
});


function changebranchrequest(){
	$(document).ready(function() {
		var roll = $("#roll_no").val(); 
		var currbranch = $("#curr_branch").val();
		var name = $("#name_s").val();
		var cpi = $("#cpi").val();
		var category = $("#category").val(); 
		var prefernce1= $("#prefernce1").val();
		var prefernce2 = $("#prefernce2").val();
		var prefernce3 = $("#prefernce3").val();
		var prefernce4 = $("#prefernce4").val();
		
		//alert(dname);
	 	debugger
        	$.ajaxSetup({
              		headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
		});

       
		$.post( "/bcrequest",{roll:roll, currbranch:currbranch, name:name, cpi:cpi, category:category, prefernce1:prefernce1, prefernce2:prefernce2, prefernce3:prefernce3, prefernce4:prefernce4} ,function( data ) {
				alert("your response is recorded ");
		});
	});
}
</script>

<div class="join-us-section" id="join-us-section">
	<div class="join-us-subheading">
		Branch change for 2015
	</div>
	<div class="partner-signup-form">
		
					<label for="Roll No">Roll No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input type="text" id="roll_no" name="partner-name" placeholder="Roll No" class="input-field">
				
					<br>
				
						<label for="Name">Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input type="text" id="name_s" name="activities" placeholder="Name" class="input-field">

					<br>
					<label for="Present Branch">Present Branch&nbsp;&nbsp;&nbsp;&nbsp;</label><select id ="curr_branch" name="Present Branch">
					    <option value="AE B.Tech">AE B.Tech</option>
					    <option value="CL B.Tech">CL B.Tech</option>
						<option value="CE B.Tech">CE B.Tech</option>
						<option value="CS B.Tech">CS B.Tech</option>
						<option value="EE B.Tech">EE B.Tech</option>
						<option value="EP B.Tech">EP B.Tech</option>
						<option value="ME B.Tech">ME B.Tech</option>
						<option value="MM B.Tech">MM B.Tech</option>
						<option value="EE Dual Deg E1">EE Dual Deg E1</option>
						<option value="EE Dual Deg E2">EE Dual Deg E2</option>
						<option value="EN Dual Deg">EN Dual Deg</option>
						<option value="EP Dual Deg">EP Dual Deg</option>
						<option value="ME Dual Deg">ME Dual Deg</option>
						<option value="MM Dual Deg Y1">MM Dual Deg Y1</option>
						<option value="EE Dual Deg Y2">EE Dual Deg Y2</option>
						<option value="CL Dual Deg">CL Dual Deg</option>
						<option value="CH">CH</option>  
					</select>

					<br>
				
					<label for="CPI">CPI&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input type="text" id="cpi" name="name" placeholder="CPI" class="input-field">

				<br>
				<label for="Category">Category&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
				<select id="category" name="Category">
					<option value="" selected="selected">Select Category</option>
				    <option value="GE">GE</option>
				    <option value="OBC">OBC</option>
					<option value="Pwd">Pwd</option>
					<option value="SC">SC</option>
					<option value="ST">ST</option>
					  
  				</select>

				<br>
				<label for="Roll No">Preference1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
				<select id="prefernce1">
					<option value="" selected="selected">Select preference</option>

				    <option value="AE B.Tech">AE B.Tech</option>
				    <option value="CL B.Tech">CL B.Tech</option>
					<option value="CE B.Tech">CE B.Tech</option>
					<option value="CS B.Tech">CS B.Tech</option>
					<option value="EE B.Tech">EE B.Tech</option>
					<option value="EP B.Tech">EP B.Tech</option>
					<option value="ME B.Tech">ME B.Tech</option>
					<option value="MM B.Tech">MM B.Tech</option>
					<option value="EE Dual Deg E1">EE Dual Deg E1</option>
					<option value="EE Dual Deg E2">EE Dual Deg E2</option>
					<option value="EN Dual Deg">EN Dual Deg</option>
					<option value="EP Dual Deg">EP Dual Deg</option>
					<option value="ME Dual Deg">ME Dual Deg</option>
					<option value="MM Dual Deg Y1">MM Dual Deg Y1</option>
					<option value="EE Dual Deg Y2">EE Dual Deg Y2</option>
					<option value="CL Dual Deg">CL Dual Deg</option>
					<option value="CH">CH</option>  
				</select>
				<br>
				<label for="Roll No">Preference2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
				<select id="prefernce2" >
					<option value="" selected="selected">Select preference</option>

				    <option value="AE B.Tech">AE B.Tech</option>
				    <option value="CL B.Tech">CL B.Tech</option>
					<option value="CE B.Tech">CE B.Tech</option>
					<option value="CS B.Tech">CS B.Tech</option>
					<option value="EE B.Tech">EE B.Tech</option>
					<option value="EP B.Tech">EP B.Tech</option>
					<option value="ME B.Tech">ME B.Tech</option>
					<option value="MM B.Tech">MM B.Tech</option>
					<option value="EE Dual Deg E1">EE Dual Deg E1</option>
					<option value="EE Dual Deg E2">EE Dual Deg E2</option>
					<option value="EN Dual Deg">EN Dual Deg</option>
					<option value="EP Dual Deg">EP Dual Deg</option>
					<option value="ME Dual Deg">ME Dual Deg</option>
					<option value="MM Dual Deg Y1">MM Dual Deg Y1</option>
					<option value="EE Dual Deg Y2">EE Dual Deg Y2</option>
					<option value="CL Dual Deg">CL Dual Deg</option>
					<option value="CH">CH</option>  
				</select>
				<br>
				<label for="Roll No">Preference3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
				<select id="prefernce3">
					<option value="" selected="selected">Select preference</option>


				    <option value="AE B.Tech">AE B.Tech</option>
				    <option value="CL B.Tech">CL B.Tech</option>
					<option value="CE B.Tech">CE B.Tech</option>
					<option value="CS B.Tech">CS B.Tech</option>
					<option value="EE B.Tech">EE B.Tech</option>
					<option value="EP B.Tech">EP B.Tech</option>
					<option value="ME B.Tech">ME B.Tech</option>
					<option value="MM B.Tech">MM B.Tech</option>
					<option value="EE Dual Deg E1">EE Dual Deg E1</option>
					<option value="EE Dual Deg E2">EE Dual Deg E2</option>
					<option value="EN Dual Deg">EN Dual Deg</option>
					<option value="EP Dual Deg">EP Dual Deg</option>
					<option value="ME Dual Deg">ME Dual Deg</option>
					<option value="MM Dual Deg Y1">MM Dual Deg Y1</option>
					<option value="EE Dual Deg Y2">EE Dual Deg Y2</option>
					<option value="CL Dual Deg">CL Dual Deg</option>
					<option value="CH">CH</option>  
				</select>
				<br>
				<label for="Roll No">Preference4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
				<select id="prefernce4" >
					<option value="" selected="selected">Select preference</option>

				    <option value="AE B.Tech">AE B.Tech</option>
				    <option value="CL B.Tech">CL B.Tech</option>
					<option value="CE B.Tech">CE B.Tech</option>
					<option value="CS B.Tech">CS B.Tech</option>
					<option value="EE B.Tech">EE B.Tech</option>
					<option value="EP B.Tech">EP B.Tech</option>
					<option value="ME B.Tech">ME B.Tech</option>
					<option value="MM B.Tech">MM B.Tech</option>
					<option value="EE Dual Deg E1">EE Dual Deg E1</option>
					<option value="EE Dual Deg E2">EE Dual Deg E2</option>
					<option value="EN Dual Deg">EN Dual Deg</option>
					<option value="EP Dual Deg">EP Dual Deg</option>
					<option value="ME Dual Deg">ME Dual Deg</option>
					<option value="MM Dual Deg Y1">MM Dual Deg Y1</option>
					<option value="EE Dual Deg Y2">EE Dual Deg Y2</option>
					<option value="CL Dual Deg">CL Dual Deg</option>
					<option value="CH">CH</option>  
				</select>


		<div class="row">
			<div class="col-xs-4 col-xs-offset-8">
				<div class="signup-field">
					<button onclick="changebranchrequest()" type="button"  id="bc_button" class="btn btn-block submit-button">SUBMIT</button>
				</div>
			</div>
		</div>
		<div id="error_div">
			<p style="color:red;"> Please enter all valid enteries</p>
		</div>
	</div>
</div>


@stop

