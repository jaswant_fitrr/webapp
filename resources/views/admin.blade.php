@extends('layouts.master')

@section('title', 'Fitrr')

@section('resources')
<?php
	 if (session_status() == PHP_SESSION_NONE) {
    session_start();
    }
   if(!isset($_SESSION['user']) || empty($_SESSION['user'])) {
        $newURL = "/";
        header('Location: '.$newURL);
        die();
    }

?>
 <meta name="_token" content="{{ csrf_token() }}"/>
	<link rel="stylesheet" href="{{ Helper::get_Path('css/partner.css') }}">
	<script src="{{ Helper::get_Path('js/partner.js') }}"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
@stop

@section('header')

@stop

@section('content')


<script>

function uploaddata(){
	$(document).ready(function(){


	

	    var file_data = $('#file2').prop('files')[0];   
		    var form_data = new FormData();                  
		    form_data.append('file', file_data);
		  
		     debugger
		        $.ajaxSetup({
		                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
		        });

		   $.ajax({
			    url : "/uploaddata",
			    type: "POST",
			    data : form_data,
			    processData: false,
			    contentType: false,
			    success:function(data, textStatus, jqXHR){
			       location.reload(); 
			    },
			    error: function(jqXHR, textStatus, errorThrown){
			        //if fails     
			       
			    }
			});

	});
}

function uploaddata1(){
	$(document).ready(function(){


	

	    var file_data = $('#file1').prop('files')[0];   
		    var form_data = new FormData();                  
		    form_data.append('file', file_data);
		  
		     debugger
		        $.ajaxSetup({
		                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
		        });

		   $.ajax({
			    url : "/datainput",
			    type: "POST",
			    data : form_data,
			    processData: false,
			    contentType: false,
			    success:function(data, textStatus, jqXHR){
			       location.reload(); 
			    },
			    error: function(jqXHR, textStatus, errorThrown){
			        //if fails     
			       
			    }
			});

	});
}

function logMeOut(){
		debugger
		$.ajaxSetup({
				headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
		});

		$.post( "/logout",{} ,function( data ) {
				 window.location.replace("/");
	 	});
}

function applybranchchange(){
		debugger
		$.ajaxSetup({
				headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
		});

		$.post( "/applybranchchange",{} ,function( data ) {
				
	 	});
}



</script>

<div class="join-us-section" id="join-us-section">
	<div class="join-us-subheading">
		Welcome Admin
	</div>
	<button onclick="logMeOut()" style="float:right;" type="button" >Logout</button>

	<div class="user-profile-pic">
	<label for="Roll No">Branch Details Data &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input type="file" id="file2" />
		<br><br>
		<button onclick="uploaddata()" type="button" >Upload</button>
		<br><br>	
		<label for="Roll No">Students Branch Change Data&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input type="file" id="file1" />
		<br><br>
		<button onclick="uploaddata1()" type="button" >Upload</button>
		<br>		
		
	</div>

	<div >
	<br>
	<br>
	<br>
	<br>

		<button onclick="applybranchchange()" type="button" >Branch Change Output</button>
		 <a href="/download" class="btn btn-large pull-right"><i class="icon-download-alt"> </i>Final Branch Change Output </a>
		<br>
		<br>
		
		 <a href="/stats" class="btn btn-large pull-right"><i class="icon-download-alt"> </i>Output Branch Stats </a>
		

	</div>


</div>


@stop

