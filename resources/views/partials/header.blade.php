@if($headertype === 1)
	<header>
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<div class="row">
						<div class="col-xs-6">
							<ul class="nav navbar-nav">
								<li><a href="#" class="navbar-brand"><img src="/images/logo.png" class="header-logo"></a></li>
							</ul>
						</div>
						<div class="col-xs-4 col-xs-offset-2">
							<div class="">
								<ul class="nav navbar-nav row header-right-links">
									<li class="col-xs-4"><a href="#">Home</a></li>
									<li class="col-xs-4"><a href="#">Contacts</a></li>
									<li class="col-xs-4"><a href="#">Register</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</nav>
	</header>
@endif

@if($headertype === 2)
	<header>
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<div class="row">
						<div class="col-xs-6">
							<ul class="nav navbar-nav">
								<li><a href="#" class="navbar-brand"><img src="/images/logo.png" class="header-logo"></a></li>
								<li><a href="#" class="nav-dashboard-link">Business Dashboard</a></li>
							</ul>
						</div>
						<div class="col-xs-6">
							<div class="">
								<ul class="nav navbar-nav row header-right-links">
									<li class="col-xs-3 col-xs-offset-2"><a href="/dashboard-home/{{$id}}">Home</a></li>
									<li class="col-xs-3"><a href="/dashboard-schedule/{{$id}}">History</a></li>
									<li class="col-xs-3"><a href="/dashboard-slots/{{$id}}">Edit Slots</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</nav>
	</header>
@endif