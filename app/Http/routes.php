<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('partner');
});

Route::get('studentlogin', function() {
	return view('login');
});

Route::post('/login','loginController@login');

Route::get('branchchange', function() {
	return view('branchchange');
});


Route::get('admin', function() {
	return view('admin');
});

Route::post('/logout','loginController@logout');

Route::post('/bcrequest','loginController@bc_request');

Route::post('/uploaddata', 'loginController@uploaddata');

Route::post('/datainput', 'loginController@datainput');

Route::post('/applybranchchange', 'loginController@branchchangealgo');
Route::get('/download', 'loginController@getDownload');
Route::get('/stats', 'loginController@getstats');