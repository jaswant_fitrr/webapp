<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Log;
use Input;

class ListingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        // default page
       
        $locationArr = DB::table('area')->first();
        // default location
        $location = $locationArr->locality;

        // activity type
        $activity = "1";

        // default time duration
        $timeduration = "8:30-11:30";
        $timeArray = explode("-", $timeduration);


        // todays date for default results
        $today = date("m/d/y"); 
        $dateArray = explode("/", $today);
        $year = date('Y');
        $datestr = $year.$dateArray[0].$dateArray[1];

        //
         $defadate = $dateArray[0]."/".$dateArray[1]."/".$year;
        // areas for filters
        $areas = DB::table('area')->get();

        //activities for filters

        $activities = DB::table('activities')->get();

        //duraiotn filter

        $duration = DB::table('duration')->get();

        // query starts here
        $defaultRes= DB::table('clients')->where('clients.area','=',$location)
        ->join('client_activities', 'clients.id', '=', 'client_activities.client_id')->where('client_activities.activity_id', '=',$activity)
        ->join('slots_available', 'clients.id', '=', 'slots_available.client_id')->where('slots_available.activity_id','=', $activity)->where('slots_available.slot_date', '=', $datestr)
        ->whereBetween('startTime', [$timeArray[0], $timeArray[1]])->whereBetween('endTime', [$timeArray[0], $timeArray[1]])->get();
        // query ends


        return view('listings.index', compact(['clientsArray','defaultRes','todayDate', 'areas','activities', 'duration','location','timeduration','activity','defadate' ]));
    }


    /*Temporary function for time being*/
    public function details($id) {
        $client = DB::table('clients')->find($id);
        $images=$client->Images;
        $imageArray = explode(":", $images);
        $classes = explode(":",$client->conductsClasses); 
        $essentials = array();


        return view('listings.client-details', compact(['client','imageArray','classes']));

    }

    /*Temporary method for time being; will need to be modified*/
    public function dashboard_schedule($id) {
        $today = date("y/m/d"); 
        $dateArray = explode("/", $today);
        $year = date('Y');
        $datestr = $year.$dateArray[1].$dateArray[2];

        $totalBookings = DB::table('bookings')->where('bookings.client_id','=', $id)->join('slots_available', 'bookings.slot_id', '=', 'slots_available.id')->orderBy('slots_available.slot_date')->get();

      //  Log::info("Logging location".$datestr.'dd'.(sizeof($todaysUsers)));
        
        return view('listings.dashboard-schedule', compact(['totalBookings', 'id']));

    }

    /*Temporary method for time being; will need to be modified*/
    public function dashboard_home($id) {

        $client = DB::table('clients')->find($id);

        $today = date("y/m/d"); 
        $dateArray = explode("/", $today);
        $year = date('Y');
        $datestr = $year.$dateArray[1].$dateArray[2];

        $todaysUsers = DB::table('bookings')->where('bookings.client_id','=', $id)->join('slots_available', 'bookings.slot_id', '=', 'slots_available.id')->where('slots_available.slot_date' , '=', $datestr)->get();
        Log::info("Logging location".$datestr.'dd'.(sizeof($todaysUsers)));

        return view('listings.dashboard-home', compact(['client','todaysUsers', 'id']));
    }

    /*Temporary method for time being; will need to be modified*/
    public function dashboard_slots($id) {
        $client_slots = DB::table('slots_available')->where('client_id', '=', $id)->join('activities', 'slots_available.activity_id', '=', 'activities.id' )->orderBy('slots_available.slot_date')->get();
        return view('listings.dashboard-slots', compact(['client_slots', 'id']));
    }

    // add a new slot

    public function addnewslot($id){

            $startTime =Input::get('startTime');
            $endTime = Input::get('endTime');
            $activity = Input::get('activity');
            $startDate = Input::get('startDate');
            $endDate = Input::get('ednDate');
            $slots =Input::get('slots');
            $excludeDate = Input::get('excludeDate');
             Log::info("Logging location".$startTime);


             $res = DB::table('slots_available')->insertGetId(
                     array('id' => null, 'client_id'  =>  $id,  'activity_id'    => $activity, 'slot_date'  =>  $startDate,  'startTime' => $startTime , 'endTime' => $endTime, 'no_slots' => $slots, 'womens_only' => "no", 'created_at', 'updated_at', 'endDate' => $endDate, 'ExcludeDate' => $excludeDate)
                     );

            // DB::unprepared( $query );

            return 1;

        }


    // filter application function
    public function applyFilter(){

        Log::info("Logging ");

        $location = Input::get('filter_location');
        $activity = Input::get('filter_activity');
        $duration = Input::get('filter_timing');
        $timeArray = explode("-", $duration);
        $date = Input::get('date_filter');
        $dateArray = explode("/", $date);
        $datestr = $dateArray[2].$dateArray[0].$dateArray[1];
       //$resArray = DB::table('clients')->where('area','=',$location)->where('')->get();

        // $resArray = DB::table('clients')->join('client_activities', function($join)
        // {
        //      $location = Input::get('filter_location');
        //      $activity = Input::get('filter_activity');
        //     $join->on('clients.id', '=', 'client_activities.client_id')->where('clients.area','=',$location)->where('client_activities.activity_id', '=',$activity);
        //         // ->where('clients.area','=',$location);
        // })->join('slots_available', function($join)
        // {
        //     $location = Input::get('filter_location');
        //     $activity = Input::get('filter_activity');
        //      $duration = Input::get('filter_timing');
        //      $timeArray = explode("-", $duration);
        //     Log::info("Logging location".$location.$activity."ddd".$timeArray[0]."ddd".$timeArray[1]);
        //     $join->on('clients.id', '=', 'slots_available.client_id')->where('slots_available.startTime','<',$timeArray[0]);
        //         // ->where('clients.area','=',$location);
        // })->get();

        $subtypes = DB::table('activities')->where('id', '=' , $activity)->get();
        $subtypesStr = $subtypes[0]->subtypes;
        $subtypeArray = explode(":", $subtypesStr); 

        Log::info("Logging location".$location.'  '.$activity.'  '.$timeArray[0].'  '.$timeArray[1].'  '.$datestr);

        if($activity!="0"){

            $resArray= DB::table('clients')->where('clients.area','=',$location)
            ->join('slots_available', 'clients.id', '=', 'slots_available.client_id')->where('slots_available.activity_id','=', $activity)->where('slots_available.slot_date', '<', $datestr)->where('slots_available.endDate', '>', $datestr)
            ->where('slots_available.startTime', '>', $timeArray[0])->where('slots_available.endTime', '<', $timeArray[1])
            ->get();
        }
        else{
              $resArray= DB::table('clients')->where('clients.area','=',$location)
            ->join('slots_available', 'clients.id', '=', 'slots_available.client_id')->where('slots_available.slot_date', '<', $datestr)->where('slots_available.endDate', '>', $datestr)
            ->where('slots_available.startTime', '>', $timeArray[0])->where('slots_available.endTime', '<', $timeArray[1])
            ->get();

        }
                      
                          
        // do not remove
      /*   $resArray= DB::table('clients')->where('clients.area','=',$location)
        ->join('client_activities', 'clients.id', '=', 'client_activities.client_id')->where('client_activities.activity_id', '=',$activity)
        ->join('slots_available', 'clients.id', '=', 'slots_available.client_id')->where('slots_available.activity_id','=', $activity)->where('slots_available.slot_date', '=', $datestr)
        ->whereBetween('startTime', [$timeArray[0], $timeArray[1]])->whereBetween('endTime', [$timeArray[0], $timeArray[1]])->get();*/

        //$resArray =DB::table('clients')->join('client_activities', 'clients.id', '=', 'client_activities.client_id')->join('slots_available', 'client_activities.client_id', '=', 'slots_available.client_id')->where('clients.area','=',$location)->get();
       
        $myres['resArray'] = $resArray;
        $myres['subtypes'] = $subtypeArray; 

         return $myres;


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
