<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use DB;
use Log;
use Response;
session_start();



    $alpha=0.75;
    $beta=1.1;
    $bcStrength = array();
    $studentReq = array();
    $afterchange= array();

  

class loginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

   public   function mycomp($a,$b){
        return($a[3] < $b[3])? 1 : -1;
    }
  public   function checkseat_avail($branch){

        global $bcStrength;
        global $beta;
        $sanct=round((float)$bcStrength[$branch][1] * $beta);
        $curre=(float)$bcStrength[$branch][2];


        if( $sanct >= $curre+1) return 1;
        return 0;

    }
   public  function capacity_check($branch,$newbranch){
        global $bcStrength;
        global $alpha;
        $sanct=round((float)$bcStrength[$branch][1] * $alpha);
        $curre=(float)$bcStrength[$branch][2];
        if( $sanct > $curre-1) {
            $bcStrength[$newbranch][3]=0;//lock the branch
            return 0;
        }

        return 1;
    }
  public   function fairness($branch){
        global $bcStrength;
        return $bcStrength[$branch][3];
    }
    
 public function branchchangealgo(){

    
        $bcsize=0;

        if (($handle = fopen("bc_strength.csv", "r")) !== FALSE) {
              //  fgetcsv($handle, 1000, ",");
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                array_push($data,1,"-1");//least CPI who got rejected ,//least cpi who got selected
                $bcStrength[$data[0]] = $data;
                $bcsize++;
            }
            fclose($handle);
        }
       // echo $bcsize;
        
        if (($handle = fopen("mycsv.csv", "r")) !== FALSE) {
          // fgetcsv($handle, 1000, ",");
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                if($data[4]=="GE" && (float)$data[3]<8) {
                    echo $data[0].",".$data[1].":".$data[2]."->"."ineligable"."\n";
                    continue;
                }
                if($data[4]=="OBC" && (float)$data[3]<8){ 
                    echo $data[0].",".$data[1].":".$data[2]."->"."ineligable"."\n";
                    continue;
                }
                if($data[4]=="SC" && (float)$data[3]<7){ 
                    echo $data[0].",".$data[1].":".$data[2]."->"."ineligable"."\n";
                    continue;
                }
                if($data[4]=="ST" && (float)$data[3]<7) {
                    echo $data[0].",".$data[1].":".$data[2]."->"."ineligable"."\n";
                    continue;
                }
                if($data[4]=="PwD" && (float)$data[3]<7) {
                    echo $data[0].",".$data[1].":".$data[2]."->"."ineligable"."\n";
                    continue;
                }
                $studentReq[] = $data;
            }
            fclose($handle);
        }


        $output_file = fopen("output.csv", "w");
        

        usort($studentReq,array($this,"mycomp"));
        //echo $studentReq[0][3];
        $endloop=0;
        $allocated = array_fill(0, sizeof($studentReq), 1000);
        while(!$endloop){
            
            for($i=0 ; $i<sizeof($studentReq); $i++){

                    if($allocated[$i]==5)continue;//if alloted first pref then ditch

                
                    $endloop=1;//default to end the loop
                    ///check set 1 


                    if ((float)$studentReq[$i][3]>=9){

                        for($j=5;$j<sizeof($studentReq[$i]) && $j<$allocated[$i];$j++){
                            $cbranch=$studentReq[$i][2];
                            $newbranch=$studentReq[$i][$j];
                            if($this->checkseat_avail($newbranch) || ((float)$bcStrength[$newbranch][4]==$studentReq[$i][3])){
                                $allocated[$i]=$j;
                                $endloop=0;

                                $bcStrength[$cbranch][2]=(int)$bcStrength[$cbranch][2]-1;//update current strength
                                $bcStrength[$newbranch][2]=(int)$bcStrength[$newbranch][2]+1;//update new branch strength
                                
                                $bcStrength[$newbranch][4]=$studentReq[$i][3];
                                echo $studentReq[$i][0].":".$cbranch."->".$newbranch."\n";

                                break;
                            }


                        }
                        //echo $studentReq[$i][3]."more\n";
                                     
                    }
                    ///check set 2
                    else {
                            for($j=5;$j<sizeof($studentReq[$i]) && $j<$allocated[$i];$j++){
                        
                                $cbranch=$studentReq[$i][2];
                                $newbranch=$studentReq[$i][$j];
                                if(($this->checkseat_avail($newbranch) && $this->capacity_check($cbranch,$newbranch) && $this->fairness($newbranch)) || ((float)$bcStrength[$newbranch][4]==$studentReq[$i][3])){
                                    $allocated[$i]=$j;
                                    $endloop=0;

                                    

                                    $bcStrength[$cbranch][2]=(int)$bcStrength[$cbranch][2]-1;//update current strength
                                    $bcStrength[$newbranch][2]=(int)$bcStrength[$newbranch][2]+1;//update new branch strength
                                    
                                   $bcStrength[$newbranch][4]=$studentReq[$i][3];
                                    Log::info($studentReq[$i][0].":".$cbranch."->".$newbranch."\n");
                                    $txt=$studentReq[$i][0].":".$cbranch."->".$newbranch."\n";
                                    fwrite($output_file, $txt);

                                    break;
                                }
                            }
                    }

            }

          foreach($bcStrength as $key){
                     $key[3]="1";
           //fwrite($output1_file, $txt);
                }
        }




        $output1_file=fopen("output_branch_stats.csv", "w");
        fwrite($output1_file, "branch_name sactioned after_branch_change\n");
        foreach($bcStrength as $key){
           $txt=$key[0].",".$key[1].",".$key[2]."\n";
           fwrite($output1_file, $txt);
        }
        
        fclose($output1_file);
        fclose($output_file);


}


    public function login(){

            $username=Input::get('username');
            $password = Input::get('password');
            $loginData = DB::table('students')->where('students.userid','=', $username)->where('students.password', '=', $password)->get();
            $myres['success'] = sizeof($loginData);
            $myres['user'] = $loginData; 

            if(sizeof($loginData)==1){
                if($username=="admin"){
                 $_SESSION['user'] = $loginData[0]->userid;
                 Log::info("Logging: ".sizeof($loginData));
                }
            }
            return $myres;
    }
    public function bc_request(){
            $roll = Input::get('roll');
            $currbranch = Input::get('currbranch');
            $name = Input::get('name');
            $cpi = Input::get('cpi');
            $category = Input::get('category');
            $prefernce1= Input::get('prefernce1');
            $prefernce2 = Input::get('prefernce2');
            $prefernce3 = Input::get('prefernce3');
            $prefernce4 =Input::get('prefernce4');
            $preferences =$prefernce1.':'.$prefernce2.':'.$prefernce3.':'.$prefernce4;

             $res = DB::table('bcrequest')->insertGetId(
                     array('id' => null, 'userid' =>$roll,'rollnumber'  =>  $roll,  'name'    => $name, 'currentbranch' => $currbranch, 'cpi' =>$cpi, 'category' => $category, 'preferences' =>$preferences )
                    );

             $res = DB::table('students')->where('userid', '=' , $roll)->get();
            if(sizeof($res) > 0 ){

            }
            else{
                    $res = DB::table('students')->insertGetId(
                     array('id' => null, 'userid' =>$roll,'password'  =>  $roll)
                    ); 
            }

            
             //Log::info("Logging: ".$roll."        ".$prefernce4."       SMS");
        //alert(dname);

    }

     public function uploaddata(){

         
        if ( 0 < $_FILES['file']['error'] ) {
            echo 'Error: ' . $_FILES['file']['error'] . '<br>';
        }
        else {
                
                Log::info($_FILES['file']['tmp_name']);
                 move_uploaded_file($_FILES['file']['tmp_name'],'./' .$_FILES['file']['name']);
                 $fname = $_FILES['file']['name'];
                $file = fopen($fname,"r");
                  $temparray = fgetcsv($file);
                 while(!feof($file)){

                    $temparray = fgetcsv($file);

                    $branch =$temparray[0];
                     $sanction = $temparray[1];
                    $current = $temparray[2];
                   
                   

                    $res = DB::table('cbs')->insertGetId(
                     array('id' => null, 'branch' =>$branch,'sanction'  =>  $sanction,  'current'    => $current)
                    );

                    Log::info($branch);

                 }
                fclose($file);
                

                

               // move_uploaded_file($_FILES['file']['tmp_name'], $target_dir.'/'.$_FILES['file']['name']);
               // $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
                Log::info("File uploaded2");
          
        }


        return 1;


    }

     public function datainput(){

         
        if ( 0 < $_FILES['file']['error'] ) {
            echo 'Error: ' . $_FILES['file']['error'] . '<br>';
        }
        else {
                
                Log::info($_FILES['file']['tmp_name']);
                 move_uploaded_file($_FILES['file']['tmp_name'],'./' .$_FILES['file']['name']);
                 $fname = $_FILES['file']['name'];

                 $file = fopen($fname,"r");
                  $temparray = fgetcsv($file);
                 while(!feof($file)){

                    $temparray = fgetcsv($file);

                    $roll =$temparray[0];
                     $name = $temparray[1];
                    $currbranch = $temparray[2];
                   
                    $cpi = $temparray[3];
                    $category = $temparray[4];
                    $preferences = $temparray[5];

                    for($i=6; $i<sizeof($temparray); $i++){
                        if($temparray[$i] == "")
                            break;
                        $preferences = $preferences .':'. $temparray[$i];

                    }

                    $res = DB::table('bcrequest')->insertGetId(
                     array('id' => null, 'userid' =>$roll,'rollnumber'  =>  $roll,  'name'    => $name, 'currentbranch' => $currbranch, 'cpi' =>$cpi, 'category' => $category, 'preferences' =>$preferences )
                    );

                    $res = DB::table('students')->where('userid', '=' , $roll)->get();
                        if(sizeof($res) > 0 ){

                        }
                        else{
                                $res = DB::table('students')->insertGetId(
                                 array('id' => null, 'userid' =>$roll,'password'  =>  $roll)
                                ); 
                        }

                    Log::info($preferences);

                 }

               // Log::info(fgetcsv($file));
                fclose($file);
            
              
                Log::info("File uploaded1");
          
        }

        //$this->branchchangealgo();
        return 1;


    }



public function getDownload(){
        //PDF file is stored under project/public/download/info.pdf
        $file= public_path()."/". "output.csv";
        $headers = array(
              'Content-Type: application/csv',
            );
        return Response::download($file, 'output.csv', $headers);
}



public function getstats(){
       
        $file= public_path()."/". "output_branch_stats.csv";
        $headers = array(
              'Content-Type: application/csv',
            );
        return Response::download($file, 'output_branch_stats.csv', $headers);
}

 public function logout(){


        session_destroy();
        return 1;   
    }





    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
