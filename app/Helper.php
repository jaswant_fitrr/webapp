<?php

namespace App;

class Helper
{
    public static function get_Path( $path ){
        $path = app('url')->asset($path);
        return str_replace('http:', '', $path);
    }
}
